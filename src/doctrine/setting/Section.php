<?php

declare(strict_types=1);

namespace Skadmin\Setting\Doctrine\Setting;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Cache(region: 'setting')]
#[ORM\Table(name: 'core_setting_section')]
#[ORM\HasLifecycleCallbacks]
class Section
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Description;

    // description = setting-section.%s.description

    /** @var Setting[]|ArrayCollection */
    #[ORM\OneToMany(mappedBy: 'section', targetEntity: Setting::class)]
    private array|ArrayCollection|Collection $settings;

    public function __construct()
    {
        $this->settings = new ArrayCollection();
    }

    /** @return Setting[] */
    public function getSettings(): array
    {
        return $this->settings->toArray();
    }
}
