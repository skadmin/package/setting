<?php

declare(strict_types=1);

namespace Skadmin\Setting\Doctrine\Setting;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Table(name: 'core_setting')]
class Setting
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Code;
    use Entity\Value;
    use Entity\Description;

    // description = settings-setting.%s.description

    #[ORM\ManyToOne(targetEntity: Section::class, inversedBy: 'settings')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Section $section;

    public function getSection(): Section
    {
        return $this->section;
    }

    public function update(string $value): void
    {
        $this->value = $value;
    }
}
