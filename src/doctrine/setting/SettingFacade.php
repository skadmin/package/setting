<?php

declare(strict_types=1);

namespace Skadmin\Setting\Doctrine\Setting;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;

final class SettingFacade extends Facade
{
    private string $tableSection;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);

        $this->table        = Setting::class;
        $this->tableSection = Section::class;
    }

    /**
     * @return Setting[]
     */
    public function getAll(): array
    {
        return $this->em
            ->getRepository($this->table)
            ->findAll();
    }

    /**
     * @return Section[]
     */
    public function getAllSections(): array
    {
        return $this->em
            ->getRepository($this->tableSection)
            ->findAll();
    }

    public function update(int $id, string $value): Setting
    {
        $setting = $this->get($id);

        $setting->update($value);
        $this->em->persist($setting);
        $this->em->flush();

        return $setting;
    }

    protected function get(?int $id = null): Setting
    {
        $setting = parent::get($id);
        assert($setting instanceof Setting);

        return $setting;
    }

    public function findByCode(string $code): ?Setting
    {
        $criteria = ['code' => $code];

        $setting = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
        assert($setting instanceof Setting || $setting === null);

        return $setting;
    }
}
