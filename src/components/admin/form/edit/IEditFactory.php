<?php

declare(strict_types=1);

namespace Skadmin\Setting\Components\Admin;

interface IEditFactory
{
    public function create(): Edit;
}
