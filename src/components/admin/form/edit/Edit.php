<?php

declare(strict_types=1);

namespace Skadmin\Setting\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Setting\BaseControl;
use Skadmin\Setting\Doctrine\Setting\SettingFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;

use function assert;
use function intval;
use function is_int;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private SettingFacade $facadeSetting;

    public function __construct(SettingFacade $facadeSetting, Translator $translator, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facadeSetting = $facadeSetting;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        return 'setting.form.edit.title';
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');
        $template->sections = $this->facadeSetting->getAllSections();

        $template->enableEdit = $this->loggedUser->isAllowed(BaseControl::RESOURCE, Privilege::WRITE);
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $referencForm = [];
        $formSection  = $form->addContainer('section');
        foreach ($this->facadeSetting->getAll() as $setting) {
            $sectionId = $setting->getSection()->getId();
            assert(is_int($sectionId));
            if (! isset($referencForm[$sectionId])) {
                $referencForm[$sectionId] = $formSection->addContainer($sectionId);
            }

            $_container = &$referencForm[$sectionId];
            assert($_container instanceof Container);
            $name = new Html();
            $name->setText($setting->getName())
                ->addText(' ');

            $title   = new SimpleTranslation('code: %s', [$setting->getCode()]);
            $popover = Html::el('sup', [
                'class'          => 'fas fa-question-circle text-primary',
                'title'          => $this->translator->translate($title),
                'data-content'   => $this->translator->translate($setting->getDescription()),
                'data-trigger'   => 'hover',
                'data-toggle'    => 'popover',
                'data-placement' => 'right',
                'data-html'      => 'true',
            ]);
            $name->addHtml($popover);

            $_container->addText((string) $setting->getId(), $name)
                ->setDefaultValue($setting->getValue())
                ->setTranslator(null);
        }

        // BUTTON
        $form->addSubmit('send', 'form.setting.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $this->onSuccess($form, $values);

        foreach ($values->section as $section) {
            foreach ($section as $settingId => $settingValue) {
                $this->facadeSetting->update(intval($settingId), (string) $settingValue);
            }
        }

        $this->onFlashmessage('form.setting.flash.success', Flash::SUCCESS);
        $this->onSubmit($form, null, $form->isSubmitted()->name);
    }
}
