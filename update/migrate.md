##### replace
```text
from: "App\Model\Doctrine\Setting\Setting"
to: "Skadmin\Setting\Doctrine\Setting\Setting"

from: "App\Model\Doctrine\Setting\Section"
to: "Skadmin\Setting\Doctrine\Setting\Section"

from: "INSERT INTO setting"
to: "INSERT INTO core_setting"

from: "INSERT INTO section"
to: "INSERT INTO core_setting_section"
```

##### move
```text
from: "update/migrations/Version20190805075323" 
to: "app/migrations/after"
```

##### modify text
```text
in "app/modules/adminModule/templates/_block/sidebar.latte" change link to setting
```

##### delete
```text
file: "app/migrations/core/2019/08/Version20190805075322"
file: "app/modules/adminModule/presenters/SettingPresenter.php"

dir: "app/modules/adminModule/components/form/formSetting"
dir: "app/modules/adminModule/templates/Setting"
dir: "app/model/doctrine/setting"
```

##### remove text
```text
from: "migrations/core/2019/07/Version20190729070719.php"
[
    'name'        => 'setting',
    'title'       => 'role-resource.setting.title',
    'description' => 'role-resource.setting.description',
],
```

##### SQL
```sql
# BEFORE
INSERT INTO _nettrine_migrations_after (version, executed_at, execution_time) VALUES ('Migrations\\Version20190805075323', now(), NULL);

# AFTER
INSERT INTO core_setting_section (webalize, name, description) SELECT webalize, name, description FROM section;
INSERT INTO core_setting (section_id, name, code, value, description) SELECT section_id, name, code, value, description FROM setting;

ALTER TABLE setting DROP FOREIGN KEY FK_9F74B898D823E37A;
DROP TABLE section;
DROP TABLE setting;

DELETE FROM _nettrine_migrations WHERE version LIKE '%20190805075322';
```

##### check resource
```text
check duplicite resource "resource"
```
