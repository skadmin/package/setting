<?php

declare(strict_types=1);

namespace DIS\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190805075323 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $sections = [
            [
                'id'          => 1,
                'name'        => 'setting-section.google.name',
                'webalize'    => 'google',
                'description' => 'setting-section.google.description',
            ],
        ];

        foreach ($sections as $section) {
            $this->addSql('INSERT INTO core_setting_section (id, webalize, name, description) VALUES (:id, :webalize, :name, :description)', $section);
        }

        $settings = [
            [
                'section_id'  => 1,
                'name'        => 'Google Analytics',
                'code'        => 'googleAnalytics',
                'value'       => '',
                'description' => 'setting-setting.google-analytics.description',
            ],
            [
                'section_id'  => 1,
                'name'        => 'Google Tag Manager',
                'code'        => 'googleTagManager',
                'value'       => '',
                'description' => 'setting-setting.google-tag-manager.description',
            ],
        ];

        foreach ($settings as $setting) {
            $this->addSql('INSERT INTO core_setting (section_id, name, code, value, description) VALUES (:section_id, :name, :code, :value, :description)', $setting);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
